# Shell

使用默认应用程序管理文件和 url

## 方法

- 打开外部连接

```javascript
horse.shell.openExternal({
  target: "https://gitee.com/horsejs/horsejs#%E6%96%87%E6%A1%A3",
  workingDir: "",
});
```
