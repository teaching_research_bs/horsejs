export class DialogConfig {
  title: string
  defaultFilePath: string
  multiSelections: boolean
  filters: [string]
  lastFilterIndex: number
}
